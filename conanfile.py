from conans import ConanFile, CMake, tools
from conans.tools import os_info, SystemPackageTool


class LefdefConan(ConanFile):
    name = "lefdef"
    version = "5.8.29"
    license = "apache"
    author = "Bernardo Ferrari Mendonca bernardo.mferrari@gmail.com"
    url = "https://gitlab.com/tarberd/lefdef"
    description = "Lef and Def libraries and binaries made by Cadence patched to work on modern systems."
    topics = ("lef", "def", "eda")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    requires = "zlib/[>=1.2.11]@conan/stable"
    #  build_requires = "bison/[>=3.0.5]@bincrafters/stable"
    exports_sources = ["def/*",
        "lef/*",
        "CMakeLists.txt",
        "lefdef-config.cmake",
        "lefdefReadme.txt",
        "README.md" ]

    def system_requirements(self):
        installer = SystemPackageTool()

        if os_info.linux_distro == "ubuntu":
            installer.install("bison")
            installer.install("libbison-dev")
        else:
            installer.install("bison")


    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()


    def package(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.install()
