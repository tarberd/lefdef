find_package(BISON)

add_custom_command(
    OUTPUT
    ${CMAKE_CURRENT_BINARY_DIR}/lef.tab.cpp
    ${CMAKE_CURRENT_BINARY_DIR}/lef.tab.h
    COMMAND ${BISON_EXECUTABLE} -v -plefyy -d ${CMAKE_CURRENT_SOURCE_DIR}/lef.y
    COMMAND ${CMAKE_COMMAND} -E copy
        ${CMAKE_CURRENT_BINARY_DIR}/lef.tab.c
        ${CMAKE_CURRENT_BINARY_DIR}/lef.tab.cpp
    COMMAND ${CMAKE_COMMAND} -E remove -f
        ${CMAKE_CURRENT_BINARY_DIR}/lef.tab.c
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
)

set(PUBLIC_HDRS
    lefiArray.hpp
    lefiCrossTalk.hpp
    lefiDebug.hpp
    lefiDefs.hpp
    lefiEncryptInt.hpp
    lefiKRDefs.hpp
    lefiLayer.hpp
    lefiMacro.hpp
    lefiMisc.hpp
    lefiNonDefault.hpp
    lefiProp.hpp
    lefiPropType.hpp
    lefiUnits.hpp
    lefiUser.hpp
    lefiUtil.hpp
    lefiVia.hpp
    lefiViaRule.hpp
    lefrCallBacks.hpp
    lefrData.hpp
    lefrReader.hpp
    lefrSettings.hpp
    lefwWriter.hpp
    lefwWriterCalls.hpp
)

set(LIBSRCS
    crypt.cpp
    ${CMAKE_CURRENT_BINARY_DIR}/lef.tab.cpp
    lef_keywords.cpp
    lefiArray.cpp
    lefiCrossTalk.cpp
    lefiDebug.cpp
    lefiEncryptInt.cpp
    lefiLayer.cpp
    lefiMacro.cpp
    lefiMisc.cpp
    lefiNonDefault.cpp
    lefiProp.cpp
    lefiPropType.cpp
    lefiTBExt.cpp
    lefiUnits.cpp
    lefiVia.cpp
    lefiViaRule.cpp
    lefrCallbacks.cpp
    lefrData.cpp
    lefrReader.cpp
    lefrSettings.cpp
    lefwWriter.cpp
    lefwWriterCalls.cpp
)

add_library(lef ${LIBSRCS})

target_include_directories(lef PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
    $<INSTALL_INTERFACE:include>
)

set_target_properties(lef PROPERTIES
    VERSION ${PROJECT_VERSION}
    SOVERSION ${PROJECT_VERSION}
)

include(GNUInstallDirs)

install(TARGETS lef
    DESTINATION ${CMAKE_INSTALL_LIBDIR}
    EXPORT lefdef-targets
)

install(FILES ${PUBLIC_HDRS}
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)
