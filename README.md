# This is a Cmake port for the lefdef cadense tollkit

Downloaded from the [Si2 OpenAccess website](https://projects.si2.org/si2_home.php).

Ported LEF/DEF v5.8-p029 to suport modern cmake build system.

# Changes:

def/def/defiPath.cpp line 319:
```
<<<
===
int defiPath::getRectMask() const {
    return 0;
}
>>>
```

lef/lef/xlefwWriterCalls.cpp line 97:
```
<<<
    LefDefParser::lefwSetAntennaInputGateAreaCbk((LefDefParser::lefwVoidCbkFnType) p0);
===
    LefDefParser::lefwAntennaInputGateAreaCbk((LefDefParser::lefwVoidCbkFnType) p0);
>>>
```

lef/lef/xlefwWriterCalls.cpp line 101:
```
<<<
    LefDefParser::lefwSetAntennaInOutDiffAreaCbk((LefDefParser::lefwVoidCbkFnType) p0);
===
    LefDefParser::lefwAntennaInOutDiffAreaCbk((LefDefParser::lefwVoidCbkFnType) p0);
>>>
```

lef/lef/xlefwWriterCalls.cpp line 105:
```
<<<
    LefDefParser::lefwSetAntennaOutputDiffAreaCbk((LefDefParser::lefwVoidCbkFnType) p0);
===
    LefDefParser::lefwAntennaOutputDiffAreaCbk((LefDefParser::lefwVoidCbkFnType) p0);
>>>
```

def/def/def.y line 470 was modified:
```
<<<
property_type_and_val: K_INTEGER { defData->real_num = 0 ;} opt_range opt_num_val
===
property_type_and_val: K_INTEGER { defData->real_num = 0 } opt_range opt_num_val
>>>
```

def/def/def.y line 475 was modified:
```
<<<
        | K_REAL { defData->real_num = 1 ;} opt_range opt_num_val
===
        | K_REAL { defData->real_num = 1 } opt_range opt_num_val
>>>
```

lef/lef/lef.y line 6438 was modified:
```
<<<
      lefData->lefPropDefType = 'I';
===
      lefData->lefPropDefType = 'I'
>>>
```

lef/lef/lef.y line 6448 was modified:
```
<<<
      lefData->lefPropDefType = 'S';
===
      lefData->lefPropDefType = 'S'
>>>
```

lef/lef/lef.y line 6453 was modified:
```
<<<
      lefData->lefPropDefType = 'Q';
===
      lefData->lefPropDefType = 'Q'
>>>
```

lef/lef/lef.y line 6458 was modified:
```
<<<
      lefData->lefPropDefType = 'S';
===
      lefData->lefPropDefType = 'S'
>>>
```
